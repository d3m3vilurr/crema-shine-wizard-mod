package com.melon.wizard;

import java.io.IOException;
import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class WizardMenu extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	                         WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    if (checkAndUpdateTime()) {
			Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_HOME);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		} else {
			try {
				refresh1();
			} catch(Exception e) {
				refresh2();
			}
		}
		this.finish();
	}

	private void refresh1() {
		com.eink.epdc.Main epdcMain = new com.eink.epdc.Main();
		epdcMain.FullRefresh2();
	}

	private void refresh2() {
		try {
			Process process = Runtime.getRuntime().exec("/system/bin/epdblk 10");
			process.getInputStream().close();
			process.getOutputStream().close();
			process.getErrorStream().close();
			process.waitFor();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private boolean checkAndUpdateTime() {
		SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
		long lastLaunched = pref.getLong("last", 0);
		long now = System.currentTimeMillis();
		Log.d("TIME GAP", Long.toString(now - lastLaunched));
		if (now - lastLaunched < 800) {
			return true;
		}
		Editor ed = pref.edit();
		ed.putLong("last", now);
		ed.commit();
		return false;
	}

}
